const expressValidator = require('express-validator');
const passport = require("passport");
const bcrypt = require('bcrypt-nodejs');


// PASSPORT
passport.serializeUser(function (user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function (user_id, done) {
    done(null, user_id);
});

export function authenticationMiddleware(x = 0) {
    return (req, res, next) => {
        console.log(`req.session.passport.user: ${JSON.stringify(req.session.passport)}`);
        const connection = require('../config/db');
        connection.query('select status from users where user_id = ?',[req.user.user_id], function (err, rows, fields) {
            if (err) throw err;
            console.log('tt' + rows[0].status);
            if (req.isAuthenticated() && rows[0].status === x) return next();
            else if (req.isAuthenticated() && rows[0].status === 0) return res.redirect('../error');
            else res.redirect('./login')
        });
    }
}

function _authenticationMiddleware() {
    return (req, res, next) => {
        console.log(`req.session.passport.user: ${JSON.stringify(req.session.passport)}`);
        if (req.isAuthenticated()) return next();
        res.redirect('./login')
    }
}
