const mysql = require("mysql");

// database setup
const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: '',
    database: process.env.DB_NAME,
    multipleStatements: true
});

connection.connect((err) => {
    if (err) throw err;
    console.log('Connected to database');
});

module.exports = connection;

// connection.query('select * from employees', function (err, rows, fields) {
//     if (err) throw err;
//     console.log(rows)
// });

// connection.end();