const express = require('express');
const router = express.Router();

const expressValidator = require('express-validator');
const passport = require("passport");
const bcrypt = require('bcrypt-nodejs');

const admin = 1;

// REGISTER
router.get('/register', function (req, res, next) {
    res.render('./user/register', {title: 'Registration'})
});

router.post('/register', function (req, res, next) {
    req.checkBody('username', 'Username field cannot be empty.').notEmpty();
    req.checkBody('username', 'Username must be between 4-15 characters long.').len(4, 15);
    req.checkBody('email', 'The email you entered is invalid, please try again.').isEmail();
    req.checkBody('email', 'Email address must be between 4-100 characters long, please try again.').len(4, 100);
    req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody("password", "Password must include one lowercase character, one uppercase character, a number, and a special character.").matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/, "i");
    req.checkBody('passwordMatch', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody('passwordMatch', 'Passwords do not match, please try again.').equals(req.body.password);
    req.checkBody('username', 'Username can only contain letters, numbers, or underscores.').matches(/^[A-Za-z0-9_-]+$/, 'i');

    const errors = req.validationErrors();

    if (errors) {
        console.log(`errors: ${JSON.stringify(errors)}`);

        res.render('./user/register', {title: 'Registration Error', errors: errors})
    } else {
        const connection = require('../config/db');
        let sql = 'insert into users set ?';
        let post = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        };

        bcrypt.hash(post.password, null, null, function (err, hash) {
            post.password = hash;
            connection.query(sql, [post], function (err, rows, fields) {
                if (err) throw err;
                connection.query('insert into users_data value ()', function (err, rows, fields) {
                    if (err) throw err;
                    connection.query('select last_insert_id() as user_id', function (err, rows, fields) {
                        if (err) throw err;

                        const user_id = rows[0];

                        console.log(rows[0]);
                        req.login(user_id, function (err) {
                            res.redirect('/')
                        })
                    })
                })
            })
        })
    }
});

// LOGIN
router.get('/login', function (req, res, next) {
    res.render('./user/login', {title: 'login'})
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/user/profile',
    failureRedirect: '/user/login'
}));


// LOGOUT
router.get('/logout', function (req, res, next) {
    req.logout();
    req.session.destroy();
    res.redirect('/')
});

// PROFILE
router.get('/profile', authenticationMiddleware(), function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'select users.user_id, username, email, first_name, last_name, address, city, state, zipcode, phone from users inner join users_data on users.user_id = users_data.user_id where users.user_id = ?';
    let sql2 = 'select sum() from orders_details where ';
    connection.query(sql, [req.user.user_id], function (err, rows, fields) {
        if (err) throw err;
        connection.query('select DATE_FORMAT(orders.date_time, \'%m/%d/%Y %H:%i\') as date_time, order_id, total from orders where user_id = ? order by order_id desc limit 5', [req.user.user_id], function (err, r, fields) {
            if (err) throw err;
            connection.query('select DATE_FORMAT(orders.date_time, \'%m/%d/%Y %H:%i\') as date_time, order_id, total, status from orders where user_id = ?', [req.user.user_id], function (err, rr, fields) {
                if (err) throw err;
                res.render('./user/profile', {title: 'Profile', user: rows[0], orders: r, allOrders: rr})
            });
        });
    });
});

router.post('/update', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'update users_data set ? where users_data.user_id = ?';
    let post = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        zipcode: req.body.zipcode,
        phone: req.body.phone
    };
    connection.query(sql, [post, req.user.user_id], function (err, rows, fields) {
        if (err) throw err;
        res.redirect('/user/profile');
    });
});

router.get('/password', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'select * from users where user_id = ?';
    let post = req.user.user_id;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.render('./user/password', {user: rows[0]});
    });
});


router.post('/update_password', function (req, res, next) {
    req.checkBody('password', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody("password", "Password must include one lowercase character, one uppercase character, a number, and a special character.").matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/, "i");
    req.checkBody('passwordMatch', 'Password must be between 8-100 characters long.').len(8, 100);
    req.checkBody('passwordMatch', 'Passwords do not match, please try again.').equals(req.body.password);

    const errors = req.validationErrors();

    if (errors) {
        console.log(`errors: ${JSON.stringify(errors)}`);

        res.render('./user/password', {errors: errors})
    } else {
        const connection = require('../config/db');
        let sql = 'update users set ? where user_id = ?';
        let post = {
            password: req.body.password
        };

        bcrypt.hash(post.password, null, null, function (err, hash) {
            post.password = hash;
            connection.query(sql, [post, req.user.user_id], function (err, rows, fields) {
                if (err) throw err;
                res.redirect('/user/profile')
            })
        })
    }
});

router.post('/order', function (req, res, next) {
    let id = req.params.id_p;
    console.log(id);
    res.redirect('./order/' + id);
});

router.get('/order/:id_p', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'select price, quantity, product_id, (select name from products where products.product_id = orders_details.product_id) as product_name from orders_details where order_id = ?';
    let post = req.params.id_p;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.render('./user/order', {currency: '$', products: rows});
    });
});


// PASSPORT
passport.serializeUser(function (user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function (user_id, done) {
    done(null, user_id);
});

function authenticationMiddleware(x = 0) {
    return (req, res, next) => {
        console.log(`req.session.passport.user: ${JSON.stringify(req.session.passport)}`);
        const connection = require('../config/db');
        connection.query('select status from users where user_id = ?', [req.user.user_id], function (err, rows, fields) {
            if (err) throw err;
            console.log('tt' + rows[0].status);
            if (req.isAuthenticated() && rows[0].status === x) return next();
            else if (req.isAuthenticated() && rows[0].status === 0) return res.redirect('../error');
            else res.redirect('./login')
        });
    }
}

module.exports = router;