const express = require('express');
const router = express.Router();
const connection = require('../config/db');
const passport = require("passport");
const nodemailer = require('nodemailer');


// CART
let cart_data, cart = {};

router.get('/cart', function (req, res) {

    cart = req.session.cart;
    if (!cart) cart = req.session.cart = {};

    let ids = Object.keys(cart);
    console.log(ids);
    console.log(Object.values(cart));

    if (ids.length > 0) {
        connection.query('select * from products where product_id in  (?)', [ids], function (err, rows, fields) {
            if (err) throw err;
            cart_data = rows;
            // console.log(cart_data);
            res.render('./cart', {currency: '$', shipping: 6.85, cart_data: rows, cart: cart});
        })
    } else {
        res.render('./cart', {currency: '$', cart_data: cart_data});
    }
});

router.get('/clear', function (req, res) {
    cart_data = req.session.cart = {};
    res.redirect('./cart');
});

router.post('/checkout', authenticationMiddleware(), function (req, res) {

    let ids = Object.keys(cart);
    let sql = 'insert into orders(user_id) values (?)';
    let post_u = req.user.user_id;
    connection.query(sql, [post_u], function (err, rows, fields) {
        if (err) throw err;
        let last = rows.insertId;

        var sum = 0;
        for (let i = 0; i < ids.length; i++) {
            connection.query('select price, stock from products where product_id = ? ', [Object.keys(cart)[i]], function (err, r, fields) {
                if (err) throw err;
                console.log(r[0]);
                let sum = 0;
                let sql = 'insert into orders_details set ?';
                let price = r[0].price;
                let quantity = Object.values(cart)[i];
                let total = price * quantity;
                let stock = r[0].stock;
                let post = {
                    order_id: last,
                    product_id: ids[i],
                    price: price,
                    quantity: quantity,
                    total_price: total
                };
                connection.query(sql, [post], function (err, rows, fields) {
                    if (err) throw err;
                });

                connection.query('select order_id from orders where order_id = (select max(order_id) from orders where user_id = ?)', [post_u], function (err, o, fields) {
                    if (err) throw err;
                    connection.query('UPDATE orders SET total = (select sum(total_price) from orders_details as total where order_id = ? ) WHERE order_id = ?', [o[0].order_id, o[0].order_id], function (err, rows, fields) {
                        if (err) throw err;
                    })
                });
                connection.query('UPDATE products SET stock = ? WHERE product_id = ?', [stock - quantity, ids[i]], function (err, rows, fields) {
                    if (err) throw err;
                });
            })
        }
    });
    cart_data = req.session.cart = {};
    res.redirect('./user/profile');
});

router.post('/test', function (req, res, next) {
    console.log('qweqwe');
    for (let i = 0; i < Object.keys(cart).length; i++) {
        console.log(Object.keys(cart));
        connection.query('select price from products where product_id = ?', [Object.keys(cart)[i]], function (err, rows, fields) {
            if (err) throw err;
            console.log(rows[0].price);
        });
    }
    res.redirect('./cart')
});

// HOME PAGE
router.get('/', function (req, res, next) {

    let sql = 'select * from products order by stock desc limit 6';

    connection.query(sql, function (err, rows, fields) {

        res.render('./index', {title: 'Home page', currency: '$', products: rows});
    });
});


// SEARCH BAR
router.get('/search', function (req, res) {
    let post = req.query.key;
    let sql = 'SELECT name from products where name like "%' + [post] + '%"'
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        var data = [];
        for (i = 0; i < rows.length; i++) {
            data.push(rows[i].name);
        }
        res.end(JSON.stringify(data));
    });
});

router.post('/search', function (req, res) {
    let post = req.body.search;
    let sql = 'select * from products where name like "%' + [post] + '%" or description like "%' + [post] + '%"';
    connection.query(sql, function (err, rows, fields) {
        res.render('./search', {currency: '$', products: rows});
    });
});


// PRODUCTS
router.get('/products', function (req, res, next) {

    let sql = 'select * from products';
    connection.query(sql, function (err, rows, fields) {
            if (err) throw err;
            res.render('./products', {currency: '$', products: rows});
        }
    );
});


router.post('/product', function (req, res, next) {
    let id = req.body.id_p;
    res.redirect('./product/' + id);
});

router.get('/product/:id_p', function (req, res, next) {
    let sql = 'select * from products where product_id = ?';
    let post = req.params.id_p;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.render('./product', {currency: '$', product: rows[0]});
    });
});

router.post('/product/:id_p', function (req, res, next) {
    cart = req.session.cart;
    if (!cart) cart = req.session.cart = {};

    let id = req.body.id;
    let count = parseInt(req.body.count, 10);

    cart[id] = (cart[id] || 0) + count;

    console.log(cart);
    console.log(Object.values(cart));

    let sql = 'select * from products where product_id = ?';
    let post = req.params.id_p;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.render('./product', {currency: '$', product: rows[0]});
    });

});

// ABOUT
router.get('/about', function (req, res, next) {
    res.render('./about', {title: 'Express'});
});


// CONTACT
router.get('/contact', function (req, res, next) {
    res.render('./contact');
});

router.post('/contact', function (req, res) {
    const output = ` 
         <p>New contact request</p>
         <h3>Contact Details</h3>
         <ul>
            <li>Name : ${req.body.name}</li>
            <li>Email : ${req.body.email}</li>
            <li>Subject : ${req.body.subject}</li>
         </ul>
         <h3>Message</h3>
         <p>${req.body.message}</p>
`;

    let transporter = nodemailer.createTransport({
        service: "Gmail",
        secure: true,
        auth: {
            user: process.env.CT_MAIL,
            pass: process.env.CT_PASSWORD
        }
    });
    let mailOptions = {
        from: 'Yerba Store Contact' + ' &lt;' + 'contact@contact.com' + '&gt;',
        to: process.env.CT_MAIL, // destined mail
        subject: req.body.subject,
        text: `${req.body.name} &lt; ${req.body.email} &gt; :\n  ${req.body.message}`,
        html: output
    };
    console.log(mailOptions);
    transporter.sendMail(mailOptions, function (err, response) {
        if (err) res.send('https://myaccount.google.com/lesssecureapps');
        else res.render('./contact', {msg: 'Message has been sent.'}); //res.redirect('./')
    });
});


// PASSPORT
passport.serializeUser(function (user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function (user_id, done) {
    done(null, user_id);
});

function authenticationMiddleware() {
    return (req, res, next) => {
        console.log(`req.session.passport.user: ${JSON.stringify(req.session.passport)}`);

        if (req.isAuthenticated()) return next();
        res.redirect('./user/login')
    }
}

module.exports = router;
