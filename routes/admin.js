const express = require('express');
const router = express.Router();

const expressValidator = require('express-validator');
const passport = require("passport");
const bcrypt = require('bcrypt-nodejs');

const admin = 1;


router.get('/', function (req, res, next) {
    // res.redirect('./admin/products');
    res.redirect('./admin/login');
});


// LOGIN
router.get('/login', function (req, res, next) {
    if (req.isAuthenticated()) res.redirect('./products');
    res.render('./admin/login', {title: 'login'})
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/admin/products',
    failureRedirect: '/admin/login'
}));


// LOGOUT
router.get('/logout', function (req, res, next) {
    req.logout();
    req.session.destroy();
    res.redirect('/')
});


router.use(authenticationMiddleware(admin));

// REGISTER
router.get('/register', function (req, res, next) {
    res.render('./admin/register', {title: 'Registration'})
});

router.post('/register', function (req, res, next) {

});

// PRODUCTS
router.get('/products', function (req, res, next) {
    const connection = require('../config/db');
    connection.query('select * from products', function (err, rows, fields) {
            if (err) throw err;
            res.render('./admin/products', {title: 'Products', currency: '$', products: rows});
        }
    );
});

router.post('/add', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'INSERT INTO products SET ?';
    let post = {
        product_id: null,
        name: req.body.prod_name,
        description: req.body.prod_desc,
        image: req.body.prod_img,
        price: req.body.prod_price,
        stock: req.body.prod_stock
    };
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.redirect('./products');
    });
});

router.post('/delete', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'delete from products where product_id = ?';
    let post = req.body.id_del;
    // res.redirect('./test/' + post);

    connection.query(sql, [post], function (err, rows, fields) {
            if (err) throw err;
            res.redirect('products');
        }
    );
});

router.post('/update', function (req, res, next) {
    let id = req.body.id_up;
    res.redirect('./update/' + id);
});

router.get('/update/:id_up', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'select * from products where product_id = ?';
    let post = req.params.id_up;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.render('./admin/update', {product: rows[0]});
    });
});

router.post('/update/sub', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'update products set name = ?, description = ?, image= ?, price = ?, stock = ?  where products.product_id = ?';
    let idU = req.body.prod_id;
    let post = [req.body.prod_name, req.body.prod_desc, req.body.prod_img, req.body.prod_price, req.body.prod_stock, idU];

    console.log(post);
    connection.query(sql, post, function (err, rows, fields) {
        if (err) throw err;
        res.redirect('/admin/products');
    });
});


// USERS
router.get('/users', function (req, res, next) {
    res.render('./admin/users', {title: 'Express'});
});


router.get('/orders', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'select order_id, DATE_FORMAT(orders.date_time, \'%m/%d/%Y %H:%i\') as date_time, total, status, (select username from users where users.user_id = orders.user_id) as user from orders';
    connection.query(sql, function (err, rows, fields) {
        if (err) throw err;
        console.log(rows[0]);
        res.render('./admin/orders', {title: 'Orders', currency: '$', orders: rows});
    });

});

router.post('/orders', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'update orders set status = 1 where order_id = ?';
    let post = req.body.id_up;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.redirect('./orders');
    });

});

router.post('/order', function (req, res, next) {
    let id = req.body.id_p;
    console.log(id);
    res.redirect('./order/' + id);
});

router.get('/order/:id_p', function (req, res, next) {
    const connection = require('../config/db');
    let sql = 'select price, quantity, product_id, (select name from products where products.product_id = orders_details.product_id) as product_name from orders_details where order_id = ?';
    let post = req.params.id_p;
    connection.query(sql, [post], function (err, rows, fields) {
        if (err) throw err;
        res.render('./user/order', {currency: '$', products: rows});
    });
});


// STATISTICS
router.get('/statistics', function (req, res, next) {
    const connection = require('../config/db');
    let sql1 = 'select product_id, name, price, stock from products where price=(select max(price) from products)';
    let sql2 = 'select p1.product_id, p1.name, p1.price, p1.stock from products p1 left join products p2 on p1.stock < p2.stock where p2.product_id is null';


    connection.query(sql1, function (err, rows, fields) {
        if (err) throw err;
        connection.query(sql2, function (err, rows2, fields) {
            if (err) throw err;

            res.render('./admin/statistics', {title: 'Statistics', currency: '$', products: rows, products2: rows2});

        });
    });

});

// search
// select union

// PASSPORT
passport.serializeUser(function (user_id, done) {
    done(null, user_id);
});

passport.deserializeUser(function (user_id, done) {
    done(null, user_id);
});

function authenticationMiddleware(x = 0) {
    return (req, res, next) => {
        console.log(`req.session.passport.user: ${JSON.stringify(req.session.passport)}`);
        const connection = require('../config/db');
        connection.query('select status from users where user_id = ?', [req.user.user_id], function (err, rows, fields) {
            if (err) throw err;
            console.log('tt' + rows[0].status);
            if (req.isAuthenticated() && rows[0].status === x) return next();
            else if (req.isAuthenticated() && rows[0].status === 0) return res.redirect('../error');
            else res.redirect('./login')
        });
    }
}

module.exports = router;
