let navbar = document.getElementById("navbar");
let background_height = 150;
let sticky = navbar.offsetTop + background_height;
let lastScroll = 0;

window.top.onscroll = () =>{
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
};

$(window.top).scroll(() => {
    let currentScroll = $(document).scrollTop();
    if (lastScroll < currentScroll) {
        $('.hiding-menu').hide();
        $('.hiding-menu').css("transition-duration", "0.5s");

    }
    else {
        $('.hiding-menu').show();
    }
    lastScroll = currentScroll;
});

//