$(document).ready(() => {
    /* vp_h will hold the height of the browser window */
    var vp_h = $(window).height();
    /* b_g will hold the height of the html body */
    var b_g = $('body').height();
    /* If the body height is lower than window */
    if (b_g < vp_h) {
        /* Set the footer css -> position: absolute; */
        $('footer').css("position", "absolute");
        $('footer').css("right", "0");
        $('footer').css("left", "0");
        $('footer').css("bottom", "0");
        $('body').css("min-height", $(window).height());
    }
});
