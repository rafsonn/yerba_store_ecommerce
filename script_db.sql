create table admin
(
  u_id     int auto_increment
    primary key,
  username varchar(50) not null,
  password varchar(50) not null
);

create table products
(
  product_id  int auto_increment
    primary key,
  name        varchar(150)   not null,
  description text           not null,
  image       varchar(50)    not null,
  price       decimal(15, 2) not null,
  stock       int            null
);

create table sessions
(
  session_id varchar(128) collate utf8mb4_bin not null
    primary key,
  expires    int(11) unsigned                 not null,
  data       text collate utf8mb4_bin         null
);

create table test
(
  order_id  int unsigned auto_increment
    primary key,
  user_id   int unsigned                        not null,
  date_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  total     decimal(15, 2)                      not null,
  status    tinyint(1) unsigned default '0'     not null
);

create table users
(
  user_id  int unsigned auto_increment
    primary key,
  username varchar(15)            not null,
  email    varchar(100)           not null,
  password binary(60)             not null,
  status   tinyint(1) default '0' not null,
  constraint email
  unique (email),
  constraint username
  unique (username)
);

create table orders
(
  order_id  int unsigned auto_increment
    primary key,
  user_id   int unsigned                        not null,
  date_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  total     decimal(15, 2)                      not null,
  status    tinyint(1) unsigned default '0'     not null,
  constraint fk_user_id
  foreign key (user_id) references users (user_id)
);

create index user_id
  on orders (user_id);

create table orders_details
(
  order_id    int unsigned auto_increment,
  product_id  int            not null,
  price       decimal(15, 2) not null,
  quantity    int            not null,
  total_price decimal(15, 2) not null,
  constraint fk_order_id
  foreign key (order_id) references orders (order_id),
  constraint fk_prduct_id
  foreign key (product_id) references products (product_id)
    on update cascade
    on delete cascade
);

create index orders_details_order_id_index
  on orders_details (order_id);

create index product_id
  on orders_details (product_id);

create table users_data
(
  user_id    int unsigned auto_increment
    primary key,
  first_name varchar(50) null,
  last_name  varchar(50) null,
  address    varchar(50) null,
  city       varchar(50) null,
  state      varchar(50) null,
  zipcode    varchar(10) null,
  phone      varchar(15) null,
  constraint fk__user_id
  foreign key (user_id) references users (user_id)
    on update cascade
    on delete cascade
);


