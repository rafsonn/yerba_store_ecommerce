require('dotenv').config();

const createError = require('http-errors');

const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sassMiddleware = require('node-sass-middleware');
const expressValidator = require('express-validator');

const multer = require('multer');

const flash = require('connect-flash');

// authentication packages

const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const MySQLStore = require('express-mysql-session')(session);
const bcrypt = require('bcrypt-nodejs');


////////////////////////////////////////////////////////////////
const indexRouter = require('./routes/index');
const adminRouter = require('./routes/admin');
const userRouter = require('./routes/user');
const usersRouter = require('./routes/users');


const app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true,
    sourceMap: true
}));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

let options = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'shop'
};

let sessionStore = new MySQLStore(options);

app.use(session({
    secret: 'bkjZaodnweunqx',
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(function (req, res, next) {
   res.locals.isAuthenticated = req.isAuthenticated();
   next();
});

app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/user', userRouter);

passport.use('local',new LocalStrategy(
    function (username, password, done) {
        const connection = require('./config/db');
        let sql = 'select user_id, password from users where username = ?';

        connection.query(sql, [username], function (err, rows, fields) {
            if (err) throw err;

            if (rows.length === 0) {
                done(null, false)
            } else {
                const hash = rows[0].password.toString();

                bcrypt.compare(password, hash, function (err, response) {
                    if (response === true) {
                        return done(null, {user_id: rows[0].user_id})
                    } else {
                        return done(null, false)
                    }
                });
            }
        });
    }
));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
